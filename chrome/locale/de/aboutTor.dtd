<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Über Tor">

<!ENTITY aboutTor.viewChangelog.label "Änderungsprotokoll anzeigen">

<!ENTITY aboutTor.ready.label "Entdecken. Privat.">
<!ENTITY aboutTor.ready2.label "Du bist bereit für das privateste Browsing-Erlebnis der Welt.">
<!ENTITY aboutTor.failure.label "Irgend etwas lief schief!">
<!ENTITY aboutTor.failure2.label "Tor funktioniert mit diesem Browser nicht.">

<!ENTITY aboutTor.search.label "Mit DuckDuckGo suchen">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Hast du noch Fragen dazu?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Schaue in unser Tor Browser Handbuch  »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Tor Browser-Handbuch">

<!ENTITY aboutTor.tor_mission.label "Projekt Tor ist eine in den USA als &quot;The Tor Project&quot; US 501(c)(3) registrierte nicht-kommerzielle Organisation für Menschenrechte und Freiheit, die freie und quelloffene Technologien für Anonymität und Privatsphäre entwickelt und bereitstellt und die Forschung und das öffentliche Verständnis auf diesen Gebieten voranbringt.">
<!ENTITY aboutTor.getInvolved.label "Mach mit »">

<!ENTITY aboutTor.newsletter.tagline "Erhalte die neuesten Nachrichten von Tor direkt in den Posteingang.">
<!ENTITY aboutTor.newsletter.link_text "Tor-Nachrichten abonnieren.">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor ist aufgrund von Spenden von Leuten wie dir frei nutzbar.">
<!ENTITY aboutTor.donationBanner.buttonA "Spende jetzt">

<!ENTITY aboutTor.alpha.ready.label "Testen. Gründlich.">
<!ENTITY aboutTor.alpha.ready2.label "Du bist bereit, die privateste Browsing-Erfahrung der Welt zu testen.">
<!ENTITY aboutTor.alpha.bannerDescription "Tor Browser Alpha ist eine instabile Version des Tor Browser, die du benutzen kannst, um neue Funktionen zu sehen, ihre Leistung zu testen und Feedback zu geben, bevor sie veröffentlicht wird.">
<!ENTITY aboutTor.alpha.bannerLink "Melde einen Fehler im Tor-Forum">

<!ENTITY aboutTor.nightly.ready.label "Testen. Gründlich.">
<!ENTITY aboutTor.nightly.ready2.label "Du bist bereit, die privateste Browsing-Erfahrung der Welt zu testen.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly ist eine instabile Version des Tor Browser, die du benutzen kannst, um neue Funktionen zu sehen, ihre Leistung zu testen und Feedback zu geben, bevor sie veröffentlicht wird.">
<!ENTITY aboutTor.nightly.bannerLink "Melde einen Fehler im Tor-Forum">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "STARK DURCH DATENSCHUTZ:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "WIDERSTAND">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "VERÄNDERUNG">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "FREIHEIT">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "SPENDE JETZT">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "Deine Spende wird von Friends of Tor verdoppelt, bis zu 100.000 $.">
