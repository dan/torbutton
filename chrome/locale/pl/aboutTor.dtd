<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Informacje na temat Tor'a">

<!ENTITY aboutTor.viewChangelog.label "Pokaż dziennik zmian">

<!ENTITY aboutTor.ready.label "Odkrywaj. Prywatnie.">
<!ENTITY aboutTor.ready2.label "Jesteś gotów do korzystania z najbardziej prywatnego sposobu przeglądania Internetu na świecie.">
<!ENTITY aboutTor.failure.label "Coś poszło nie tak!">
<!ENTITY aboutTor.failure2.label "Tor nie działa w tej przeglądarce.">

<!ENTITY aboutTor.search.label "Wyszukaj z DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Pytania?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Sprawdź naszą instrukcje korzystania z Tor Browser »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Instrukcja korzystania z Tor Browser">

<!ENTITY aboutTor.tor_mission.label "Projekt Tor jest organizacją non-profit US 501(c)(3), wspierającą prawa człowieka i wolności. W jego ramach tworzone są darmowe i otwarte technologie chroniące anonimowość i prywatność. Tor wspiera ich nieograniczoną dostępność i użycie oraz popularyzuje je w świecie nauki i w życiu codziennym. ">
<!ENTITY aboutTor.getInvolved.label "Zaangażuj się »">

<!ENTITY aboutTor.newsletter.tagline "Otrzymuj aktualności o Tor, prosto do swojej skrzynki.">
<!ENTITY aboutTor.newsletter.link_text "Zapisz się na newsletter.">
<!ENTITY aboutTor.donationBanner.freeToUse "Z przeglądarki Tor można korzystać bezpłatnie dzięki darowiznom od osób takich jak Ty.">
<!ENTITY aboutTor.donationBanner.buttonA "Przekaż darowiznę teraz">

<!ENTITY aboutTor.alpha.ready.label "Przetestuj. Dokładnie.">
<!ENTITY aboutTor.alpha.ready2.label "Jesteś gotów do przetestowania najbardziej prywatnego sposobu przeglądania Internetu na świecie.">
<!ENTITY aboutTor.alpha.bannerDescription "Tor Browser Alpha to niestabilna wersja Tor Browser, której możesz używać do podglądu nowych funkcji, testowania ich wydajności i przekazywania opinii przed wydaniem.">
<!ENTITY aboutTor.alpha.bannerLink "Zgłoś błąd na forum Tor">

<!ENTITY aboutTor.nightly.ready.label "Przetestuj. Dokładnie.">
<!ENTITY aboutTor.nightly.ready2.label "Jesteś gotów do przetestowania najbardziej prywatnego sposobu przeglądania Internetu na świecie.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly to niestabilna wersja Tor Browser, której możesz używać do podglądu nowych funkcji, testowania ich wydajności i przekazywania opinii przed wydaniem.">
<!ENTITY aboutTor.nightly.bannerLink "Zgłoś błąd na forum Tor">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "ZASILANE PRZEZ PRYWATNOŚĆ:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "OPÓR">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "ZMIANA">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "WOLNOŚĆ">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "ZRÓB DOTACJE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "Twoja darowizna zostanie wyrównana przez Przyjaciół Tor, do maksymalnie 100 000 USD.">
