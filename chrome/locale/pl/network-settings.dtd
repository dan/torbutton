<!ENTITY torsettings.dialog.title "Ustawienia Sieci">
<!ENTITY torsettings.wizard.title.default "Połącz z siecią Tor">
<!ENTITY torsettings.wizard.title.configure "Ustawienia sieci Tor">
<!ENTITY torsettings.wizard.title.connecting "Nawiązywanie połączenia">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Język Tor Browser">
<!ENTITY torlauncher.localePicker.prompt "Wybierz język.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Kliknij &quot;Połącz&quot;, aby połączyć się z Torem.">
<!ENTITY torSettings.configurePrompt "Kliknij &quot;Konfiguruj&quot;, aby dostosować ustawienia sieci, jeśli jesteś w kraju, który cenzuruje Tora (np. Egipt, Chiny, Turcja) lub jeśli łączysz się z prywatnej sieci, która wymaga serwera proxy.">
<!ENTITY torSettings.configure "Konfiguruj">
<!ENTITY torSettings.connect "Połącz">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Uruchamianie oprogramowania Tor">
<!ENTITY torsettings.restartTor "Zrestartuj Tora">
<!ENTITY torsettings.reconfigTor "Przekonfiguruj">

<!ENTITY torsettings.discardSettings.prompt "Skonfigurowałeś mostki Tora lub wprowadziłeś lokalne ustawienia serwera proxy.&#160; Aby nawiązać bezpośrednie połączenie do sieci Tor, ustawienia te muszą zostać usunięte.">
<!ENTITY torsettings.discardSettings.proceed "Usuń ustawienia i połącz">

<!ENTITY torsettings.optional "(opcjonalnie)">

<!ENTITY torsettings.useProxy.checkbox "Używam serwera proxy do łączenia się z Internetem">
<!ENTITY torsettings.useProxy.type "Typ serwera proxy">
<!ENTITY torsettings.useProxy.type.placeholder "wybierz typ proxy">
<!ENTITY torsettings.useProxy.address "Adres">
<!ENTITY torsettings.useProxy.address.placeholder "Nazwa hosta lub adres IP">
<!ENTITY torsettings.useProxy.port "Port">
<!ENTITY torsettings.useProxy.username "Użytkownik">
<!ENTITY torsettings.useProxy.password "Hasło">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Ten komputer pozwala na połączenie z ustalonymi portami">
<!ENTITY torsettings.firewall.allowedPorts "Dozwolone porty">
<!ENTITY torsettings.useBridges.checkbox "Tor jest ocenzurowany w moim kraju">
<!ENTITY torsettings.useBridges.default "Wybierz wbudowany mostek">
<!ENTITY torsettings.useBridges.default.placeholder "wybierz mostek">
<!ENTITY torsettings.useBridges.bridgeDB "Uzyskaj mostek od torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Wprowadź znaki z obrazka">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Poproś o inne zadanie">
<!ENTITY torsettings.useBridges.captchaSubmit "Podsumowując">
<!ENTITY torsettings.useBridges.custom "Dostarcz mostek, który znam">
<!ENTITY torsettings.useBridges.label "Wprowadź informacje o mostku z zaufanego źródła.">
<!ENTITY torsettings.useBridges.placeholder "wpisz adres:port (po jednym w każdej linii)">

<!ENTITY torsettings.copyLog "Skopiuj log do schowka">

<!ENTITY torsettings.proxyHelpTitle "Pomoc proxy">
<!ENTITY torsettings.proxyHelp1 "Lokalny serwer proxy może być potrzebny podczas łączenia się przez sieć firmową, szkolną lub uniwersytecką.&#160;Jeśli nie masz pewności, czy serwer proxy jest potrzebny, sprawdź ustawienia internetowe w innej przeglądarce lub sprawdź ustawienia sieciowe systemu.">

<!ENTITY torsettings.bridgeHelpTitle "Pomoc Przekaźników Mostkowych">
<!ENTITY torsettings.bridgeHelp1 "Mostki są specjalnymi rodzajami przekaźników, które utrudniają blokowanie połączeń z siecią Tor. Każdy rodzaj mostku wykorzystuje inną metodę unikania cenzury.&#160; Mostki &quot;obfs&quot; sprawiają, że Twój ruch wygląda jak losowy szum, a &quot;meek-azure&quot; sprawiają, że Twój ruch wygląda jakbyś łączył się z tą usługą zamiast z Torem.">
<!ENTITY torsettings.bridgeHelp2 "Ze względu na różny sposób, w jaki kraje próbują blokować Tora, niektóre mostki działają tylko w niektórych krajach.&#160; Jeśli nie masz pewności, które mostki działają w Twoim kraju, odwiedź torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Proszę zaczekać, aż ustanowimy połączenie do sieci Tor.&#160; Może to potrwać kilka minut.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Połączenie">
<!ENTITY torPreferences.torSettings "Ustawienia Tor">
<!ENTITY torPreferences.torSettingsDescription "Tor Browser kieruje Twój ruch przez sieć Tor, utrzymywaną przez tysiące wolontariuszy na całym świecie." >
<!ENTITY torPreferences.learnMore "Dowiedz się więcej">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "Testuj">
<!ENTITY torPreferences.statusInternetOnline "Online">
<!ENTITY torPreferences.statusInternetOffline "Offline">
<!ENTITY torPreferences.statusTorLabel "Sieć Tor:">
<!ENTITY torPreferences.statusTorConnected "Połączono">
<!ENTITY torPreferences.statusTorNotConnected "Brak połączenia">
<!ENTITY torPreferences.statusTorBlocked "Potencjalnie zablokowany">
<!ENTITY torPreferences.learnMore "Dowiedz się więcej">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Szybki start">
<!ENTITY torPreferences.quickstartDescriptionLong "Szybki start łączy Tor Browser z siecią Tor automatycznie po uruchomieniu, w oparciu o ustawienia ostatnio używanego połączenia.">
<!ENTITY torPreferences.quickstartCheckbox "Zawsze łącz automatycznie">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Mostki">
<!ENTITY torPreferences.bridgesDescription "Mostki ułatwiają dostęp do sieci Tor w miejscach, w których Tor jest zablokowany. W zależności od tego, gdzie jesteś, niektóre mostki mogą działać lepiej niż inne.">
<!ENTITY torPreferences.bridgeLocation "Twoja lokalizacja">
<!ENTITY torPreferences.bridgeLocationAutomatic "Automatycznie">
<!ENTITY torPreferences.bridgeLocationFrequent "Często wybierane lokalizacje">
<!ENTITY torPreferences.bridgeLocationOther "Inne lokalizacje">
<!ENTITY torPreferences.bridgeChooseForMe "Wybierz mostek dla mnie…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Twoje obecne mostki">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "Możesz zapisać jeden lub więcej mostków, a Tor wybierze, którego z nich użyć podczas połączenia. W razie potrzeby Tor automatycznie przełączy się na korzystanie z innego mostku.">
<!ENTITY torPreferences.bridgeId "#1 mostek: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Usuń">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Wyłącz wbudowane mostki">
<!ENTITY torPreferences.bridgeShare "Udostępnij ten mostek za pomocą kodu QR lub kopiując jego adres:">
<!ENTITY torPreferences.bridgeCopy "Skopiuj adres mostka">
<!ENTITY torPreferences.copied "Skopiowano!">
<!ENTITY torPreferences.bridgeShowAll "Pokaż wszystkie mostki">
<!ENTITY torPreferences.bridgeRemoveAll "Usuń wszystkie mostki">
<!ENTITY torPreferences.bridgeAdd "Dodaj nowy mostek">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Wybierz jeden z wbudowanych mostków Tor Browser">
<!ENTITY torPreferences.bridgeSelectBuiltin "Wybierz wbudowany mostek…">
<!ENTITY torPreferences.bridgeRequest "Poproś o mostek...">
<!ENTITY torPreferences.bridgeEnterKnown "Wpisz adres mostka, który już znasz">
<!ENTITY torPreferences.bridgeAddManually "Dodaj mostek ręcznie…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Zaawansowane">
<!ENTITY torPreferences.advancedDescription "Skonfiguruj sposób, w jaki Tor Browser łączy się z Internetem">
<!ENTITY torPreferences.advancedButton "Ustawienia…">
<!ENTITY torPreferences.viewTorLogs "Zobacz dziennik zdarzeń Tor">
<!ENTITY torPreferences.viewLogs "Wyświetl dziennik zdarzeń...">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Usunąć wszystkie mostki?">
<!ENTITY torPreferences.removeBridgesWarning "To działanie nie może zostać cofnięte.">
<!ENTITY torPreferences.cancel "Anuluj">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Skanuj kod QR">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Wbudowane mostki">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser zawiera określone typy mostków znane jako „podłączane transporty”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 to rodzaj wbudowanego mostka, który sprawia, że ruch w sieci Tor wygląda losowo. Są one też mniej podatne na zablokowanie niż ich poprzednicy, mostki obfs3.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake to wbudowany mostek, który omija cenzurę, kierując połączenie przez serwery proxy Snowflake, które są obsługiwane przez wolontariuszy.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure to wbudowany mostek, który sprawia, że wygląda na to, że korzystasz ze strony internetowej Microsoftu zamiast Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Poproś o mostek">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Nawiązywanie połączenia z BridgeDB. Proszę czekać.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Rozwiąż CAPTCHA, aby poprosić o mostek.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "Rozwiązanie nie jest poprawne. Proszę spróbuj ponownie.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Dostarcz mostek ">
<!ENTITY torPreferences.provideBridgeHeader "Wprowadź informacje o mostku z zaufanego źródła">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Ustawienia połączenia">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Skonfiguruj sposób, w jaki Tor Browser łączy się z Internetem">
<!ENTITY torPreferences.firewallPortsPlaceholder "Wartości oddzielone przecinkiem">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Dziennik zdarzeń Tor">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Niepołączony">
<!ENTITY torConnect.connectingConcise "Trwa łączenie...">
<!ENTITY torConnect.tryingAgain "Próbuję ponownie…">
<!ENTITY torConnect.noInternet "Tor Browser nie może połączyć się z Internetem">
<!ENTITY torConnect.noInternetDescription "Może to być spowodowane problemem z połączeniem, a nie zablokowaniem sieci Tor. Sprawdź połączenie internetowe, ustawienia proxy i zapory sieciowej przed ponowną próbą.">
<!ENTITY torConnect.couldNotConnect "Tor Browser nie może połączyć się z siecią Tor">
<!ENTITY torConnect.assistDescriptionConfigure "skonfiguruj połączenie"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "Jeśli sieć Tor jest zablokowana w Twojej lokalizacji, może pomóc wypróbowanie mostka. Asystent połączenia może wybrać jeden dla Ciebie na podstawie Twojej lokalizacji lub zamiast tego możesz #1 ręcznie."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Próbuję mostka…">
<!ENTITY torConnect.tryingBridgeAgain "Próbuję jeszcze raz…">
<!ENTITY torConnect.errorLocation "Tor Browser nie może Cię zlokalizować">
<!ENTITY torConnect.errorLocationDescription "Tor Browser musi znać Twoją lokalizację, aby wybrać dla Ciebie odpowiedni mostek. Jeśli wolisz nie udostępniać swojej lokalizacji, zamiast tego #1 ręcznie."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Czy te ustawienia lokalizacji są prawidłowe?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser nadal nie może połączyć się z siecią Tor. Sprawdź, czy ustawienia lokalizacji są prawidłowe i spróbuj ponownie lub zamiast tego #1."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.finalError "Tor Browser nadal nie może się połączyć">
<!ENTITY torConnect.finalErrorDescription "Pomimo wszelkich starań asystent połączenia nie był w stanie połączyć się z siecią Tor. Spróbuj rozwiązać problem z połączeniem i ręcznie dodać mostek.">
<!ENTITY torConnect.breadcrumbAssist "Asystent połączenia">
<!ENTITY torConnect.breadcrumbLocation "Ustawienia lokalizacji">
<!ENTITY torConnect.breadcrumbTryBridge "Wypróbuj mostek">
<!ENTITY torConnect.automatic "Automatycznie">
<!ENTITY torConnect.selectCountryRegion "Wybierz kraj lub region">
<!ENTITY torConnect.frequentLocations "Często wybierane lokalizacje">
<!ENTITY torConnect.otherLocations "Inne lokalizacje">
<!ENTITY torConnect.restartTorBrowser "Uruchom ponownie Tor Browser">
<!ENTITY torConnect.configureConnection "Skonfiguruj połączenie…">
<!ENTITY torConnect.viewLog "Wyświetl dziennik…">
<!ENTITY torConnect.tryAgain "Spróbuj ponownie">
<!ENTITY torConnect.offline "Internet nieosiągalny">
<!ENTITY torConnect.connectMessage "Zmiany w ustawieniach Tor nie zaczną obowiązywać, dopóki się nie połączysz">
<!ENTITY torConnect.tryAgainMessage "Tor Browser nie może nawiązać połączenia z siecią Tor">
<!ENTITY torConnect.yourLocation "Twoja lokalizacja">
<!ENTITY torConnect.tryBridge "Wypróbuj mostek">
<!ENTITY torConnect.autoBootstrappingFailed "Konfiguracja automatyczna nie powiodła się">
<!ENTITY torConnect.autoBootstrappingFailed "Konfiguracja automatyczna nie powiodła się">
<!ENTITY torConnect.cannotDetermineCountry "Nie można określić kraju użytkownika">
<!ENTITY torConnect.noSettingsForCountry "Brak dostępnych ustawień dla Twojej lokalizacji">
