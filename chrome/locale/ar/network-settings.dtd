<!ENTITY torsettings.dialog.title "إعدادات شبكة تور">
<!ENTITY torsettings.wizard.title.default "اتصل بتور">
<!ENTITY torsettings.wizard.title.configure "إعدادات شبكة تور">
<!ENTITY torsettings.wizard.title.connecting "يجري إنشاء اتصال">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "لغة متصفح تور">
<!ENTITY torlauncher.localePicker.prompt "الرجاء اختيار لغة.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "انقر &quot;اتصل&quot; للاتصال بتور.">
<!ENTITY torSettings.configurePrompt "انقر &quot;اضبط&quot; لضبط إعدادات الشبكة إن كنت في بلد يحجب تور (مثل مصر، والصين، وتركيا) أو إن كنت تتصل من شبكة خاصة تتطلب وسيط.">
<!ENTITY torSettings.configure "تكوين">
<!ENTITY torSettings.connect "اتصل">

<!-- Other: -->

<!ENTITY torsettings.startingTor "في انتظار تور حتي يبدء بالعمل...">
<!ENTITY torsettings.restartTor "أعِد تشغيل تور">
<!ENTITY torsettings.reconfigTor "أعِد ضبط">

<!ENTITY torsettings.discardSettings.prompt "ضبطت جسر تور أو أدخلت إعدادات الوسيط المحلي.&#160; لإنشاء اتصال مباشر بشبكه تور، يجب إزاله هذه الإعدادات.">
<!ENTITY torsettings.discardSettings.proceed "ازل الاعدادات ثم اتصل">

<!ENTITY torsettings.optional "اختياري">

<!ENTITY torsettings.useProxy.checkbox "أستخدم وسيطا للاتصال بالإنترنت">
<!ENTITY torsettings.useProxy.type "نوع البروكسي">
<!ENTITY torsettings.useProxy.type.placeholder "اختر نوع الوسيط">
<!ENTITY torsettings.useProxy.address "العنوان">
<!ENTITY torsettings.useProxy.address.placeholder "عنوان الإنترنت IP أو اسم المضيف">
<!ENTITY torsettings.useProxy.port "منفذ">
<!ENTITY torsettings.useProxy.username "اسم المستخدم">
<!ENTITY torsettings.useProxy.password "‮كلمة السّر">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "اتصال الإنترنت لهذا الكمبيوتر يمر بجدار حماية يسمح بالاتصال فقط من خلال منافذ معينة">
<!ENTITY torsettings.firewall.allowedPorts "المنافذ المسموح بها">
<!ENTITY torsettings.useBridges.checkbox "تور محجوب في بلدي">
<!ENTITY torsettings.useBridges.default "اختر جسرا مُدمَجا في البرنامج">
<!ENTITY torsettings.useBridges.default.placeholder "اختر جسرا">
<!ENTITY torsettings.useBridges.bridgeDB "اطلب جسرا من torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "أدخل الرموز الموجودة في الصورة">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "احصل على تحد جديد">
<!ENTITY torsettings.useBridges.captchaSubmit "أرسل">
<!ENTITY torsettings.useBridges.custom "أدخل عنوان جسرٍ أعرفه">
<!ENTITY torsettings.useBridges.label "أدخل معلومات جسر من مصدر موثوق.">
<!ENTITY torsettings.useBridges.placeholder "اكتب العنوان:المنفذ (واحد لكل سطر)">

<!ENTITY torsettings.copyLog "نسخ سجل تور إلي الحافظة">

<!ENTITY torsettings.proxyHelpTitle "مساعدة الوسيط">
<!ENTITY torsettings.proxyHelp1 "قد تكون هناك حاجة إلى وسيط (proxy) محلي عند الاتصال من خلال شركة أو مدرسة أو شبكة جامعية. إذا لم تكن متأكدًا مما إذا كان الوسيط مطلوبًا أم لا، فابحث عن إعدادات الإنترنت في متصفح آخر أو تحقق من إعدادات شبكة النظام لديك.">

<!ENTITY torsettings.bridgeHelpTitle "المساعدة الخاصة بالجسور المُرحلة">
<!ENTITY torsettings.bridgeHelp1 "الجسور هي تحويلات غير مدرجة تجعل حجب الاتصالات إلى شبكة تور أصعب.&#160; كل نوع من الجسور يستخدم طريقة مختلفة لتجنب الرقابة.&#160; جسور obfs تجعل حركة معلوماتك تبدو كضجيج عشوائي، وجسور meek تجعل حركة معلوماتك تبدوا كأنها تتصل لتلك الخدمة بدلا من تور.">
<!ENTITY torsettings.bridgeHelp2 "بسبب طريقة حظر بعض البلدان لتور، تعمل بعض الجسور في بلدان معينة لكنها لا تعمل ببعضها الآخر. إن لم تكن متأكدا بشأن أي الجسور تعمل في بلدك، فزر  torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "الرجاء الانتظار بينما ننشئ اتصالا لشبكة تور.&#160; قد يستغرق هذا عدة دقائق.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "الاتصال">
<!ENTITY torPreferences.torSettings "إعدادات تور Tor">
<!ENTITY torPreferences.torSettingsDescription "متصفح Tor Browser يوجه حركة بياناتك عبر شبكة تور Tor Network, التي يشغلها آلاف المتطوعون حول العالم." >
<!ENTITY torPreferences.learnMore "تعرف على المزيد">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "الإنترنت:">
<!ENTITY torPreferences.statusInternetTest "افحص">
<!ENTITY torPreferences.statusInternetOnline "متصل">
<!ENTITY torPreferences.statusInternetOffline "غير متصل">
<!ENTITY torPreferences.statusTorLabel "شبكة Tor:">
<!ENTITY torPreferences.statusTorConnected "متصل">
<!ENTITY torPreferences.statusTorNotConnected "غير متصل">
<!ENTITY torPreferences.statusTorBlocked "يحتمل أن يكون محجوب">
<!ENTITY torPreferences.learnMore "تعرف على المزيد">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "بداية سريعة">
<!ENTITY torPreferences.quickstartDescriptionLong "التشغيل السريع يجعل اتصال متصفح Tor بشبكة Tor يتم تلقائيًا عند التشغيل، اعتمادًا على إعداداتك لآخر عملية اتصال قمت بها.">
<!ENTITY torPreferences.quickstartCheckbox "اتصل تلقائيا بشكل دائم">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "الجسور">
<!ENTITY torPreferences.bridgesDescription "تساعدك الجسور على دخول شبكة تور Tor Network في الأماكن التي تحظر تور Tor. وبالاعتماد على مكان تواجدك فإن أحد الجسور قد يعمل بشكل أفضل من غيره.  ">
<!ENTITY torPreferences.bridgeLocation "موقعك الجغرافي">
<!ENTITY torPreferences.bridgeLocationAutomatic "تلقائي">
<!ENTITY torPreferences.bridgeLocationFrequent "المواقع الجغرافية المختارة بشكل متكرر:">
<!ENTITY torPreferences.bridgeLocationOther "مواقع جغرافية أخرى">
<!ENTITY torPreferences.bridgeChooseForMe "اختر جسرًا لي...">
<!ENTITY torPreferences.bridgeBadgeCurrent "جسورك الحالية:">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "يمكنك حفظ معلومات جسر أو أكثر، وسيقوم Tor باختيار أي جسر منهم يتم استخدامه عند الاتصال. Tor سيقوم باستخدام جسر آخر بشكل تلقائي عند الحاجة.">
<!ENTITY torPreferences.bridgeId "#1 جسر: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "إزالة">
<!ENTITY torPreferences.bridgeDisableBuiltIn "تعطيل الجسور المدمجة">
<!ENTITY torPreferences.bridgeShare "شارك جسرًا باستخدام رمز استجابة سريعة أو عبر نسخ عنوانه:">
<!ENTITY torPreferences.bridgeCopy "انسخ عنوان الجسر">
<!ENTITY torPreferences.copied "تم النسخ!">
<!ENTITY torPreferences.bridgeShowAll "عرض جميع الجسور">
<!ENTITY torPreferences.bridgeRemoveAll "إزالة جميع الجسور">
<!ENTITY torPreferences.bridgeAdd "أضف جسرًا جديدًا">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "اختر جسرًا من الجسور المدمجة في متصفح Tor ">
<!ENTITY torPreferences.bridgeSelectBuiltin "اختر جسرًا مدمجًا...">
<!ENTITY torPreferences.bridgeRequest "اطلب جسرا…">
<!ENTITY torPreferences.bridgeEnterKnown "أدخل عنوان الجسر الذي تريد استخدامه">
<!ENTITY torPreferences.bridgeAddManually "أضف جسرًا بشكل يدوي...">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "متقدم">
<!ENTITY torPreferences.advancedDescription "تهيأة كيف يتصل متصفح Tor بالإنترنت">
<!ENTITY torPreferences.advancedButton "الإعدادات...">
<!ENTITY torPreferences.viewTorLogs "عرض سجلات Tor">
<!ENTITY torPreferences.viewLogs "شاهد السجلات...">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "إزالة جميع الجسور؟">
<!ENTITY torPreferences.removeBridgesWarning "هذا الإجراء لا يمكن التراجع عنه.">
<!ENTITY torPreferences.cancel "إلغاء ">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "افحص رمز الاستجابة السريعة">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "الجسور المدمجة">
<!ENTITY torPreferences.builtinBridgeDescription "متصفح Tor يحتوي على أنواع معينة من الجسر معروفة باسم &quot;النواقل القابلة للتوصيل&quot;.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "هناك نوع من الجسور المدمجة يسمى obfs4 والذي يجعل حركة المرور الخاصّة بشبكة Tor تبدو عشوائية. هذا النوع من الجسور أيضًا أقل عُرضة للحجب مقارنةً بأسلافها، وهي جسور obfs3.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "جسر Snowflake المدمج يُعد قاهرًا للرقابة من خلال إعادة توجيه اتصالك عبر خوادم وكيلة، والتي تُدار بواسطة متطوّعين.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "جسر meek-azure المدمج يجعل استخدامك للإنترنت يبدو وكأنك تقوم بتصفح موقع مايكروسوفت بدلًا من استخدام شبكة Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "اطلب جسراً">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "يتصل بقاعدة بيانات الجسور BridgeDB. يرجى الانتظار.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "حل كاباتشا لطلب جسر.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "الحل ليس سليمًا. يُرجى إعادة المحاولة.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "وفر جسرًا">
<!ENTITY torPreferences.provideBridgeHeader "أدخل معلومات الجسر من مصدر موثوق">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "إعدادات الاتصال">
<!ENTITY torPreferences.connectionSettingsDialogHeader "هيئ كيفية قيام متصفح Tor بالاتصال بالإنترنت">
<!ENTITY torPreferences.firewallPortsPlaceholder "قيم مفصولة بالفاصلة">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "سجلات تور Tor">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "غير متصل">
<!ENTITY torConnect.connectingConcise "جاري الاتصال ...">
<!ENTITY torConnect.tryingAgain "Trying again…">
<!ENTITY torConnect.noInternet "Tor Browser couldn’t reach the Internet">
<!ENTITY torConnect.noInternetDescription "This could be due to a connection issue rather than Tor being blocked. Check your Internet connection, proxy and firewall settings before trying again.">
<!ENTITY torConnect.couldNotConnect "Tor Browser could not connect to Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configure your connection"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "If Tor is blocked in your location, trying a bridge may help. Connection assist can choose one for you using your location, or you can #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Trying a bridge…">
<!ENTITY torConnect.tryingBridgeAgain "Trying one more time…">
<!ENTITY torConnect.errorLocation "Tor Browser couldn’t locate you">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.finalError "Tor Browser still cannot connect">
<!ENTITY torConnect.finalErrorDescription "Despite its best efforts, connection assist was not able to connect to Tor. Try troubleshooting your connection and adding a bridge manually instead.">
<!ENTITY torConnect.breadcrumbAssist "Connection assist">
<!ENTITY torConnect.breadcrumbLocation "Location settings">
<!ENTITY torConnect.breadcrumbTryBridge "Try a bridge">
<!ENTITY torConnect.automatic "تلقائي">
<!ENTITY torConnect.selectCountryRegion "Select Country or Region">
<!ENTITY torConnect.frequentLocations "المواقع الجغرافية المختارة بشكل متكرر:">
<!ENTITY torConnect.otherLocations "مواقع جغرافية أخرى">
<!ENTITY torConnect.restartTorBrowser "Restart Tor Browser">
<!ENTITY torConnect.configureConnection "تكوين الاتصال…">
<!ENTITY torConnect.viewLog "View logs…">
<!ENTITY torConnect.tryAgain "الرجاء المحاولة مجددًا">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "لن تسري التغييرات على إعدادات Tor حتى تقوم بالاتصال">
<!ENTITY torConnect.tryAgainMessage "فشل متصفح Tor في إنشاء اتصال بشبكة Tor">
<!ENTITY torConnect.yourLocation "Your Location">
<!ENTITY torConnect.tryBridge "جرب الجسر">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.cannotDetermineCountry "Unable to determine user country">
<!ENTITY torConnect.noSettingsForCountry "No settings available for your location">
