<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "টর সম্পর্কে">

<!ENTITY aboutTor.viewChangelog.label "পরিবর্তন দেখুন">

<!ENTITY aboutTor.ready.label "এক্সপ্লোর. গোপনে.">
<!ENTITY aboutTor.ready2.label "বিশ্বের সবচেয়ে ব্যক্তিগত ব্রাউজিং অভিজ্ঞতার জন্য আপনি এখন প্রস্তুত ।">
<!ENTITY aboutTor.failure.label "কোন একটা সমস্যা হয়েছে!">
<!ENTITY aboutTor.failure2.label "টর এই ব্রাউজারে কাজ করছে না।">

<!ENTITY aboutTor.search.label "DuckDuckGo দিয়ে অনুসন্ধান">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "প্রশ্ন আছে?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "আমাদের টর ব্রাউজারের ম্যানুয়াল দেখুন »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "টর ব্রাউজার ম্যানুয়াল">

<!ENTITY aboutTor.tor_mission.label "টর প্রকল্পটি একটি মার্কিন 501(c)(3) অলাভজনক প্রতিষ্ঠান, যা পরিচয় ও গোপনীয়তা রক্ষার বিনামূল্য ও ওপেন সোর্স প্রযুক্তির (free and open source anonymity and privacy technology) সৃষ্টি ও প্রসার, সেগুলোর অবাধ সহজলভ্যতা ও ব্যবহার সমর্থন, এবং সেগুলোকে বিজ্ঞানমনস্ক ব্যক্তিবর্গ ও সাধারণ জনগণের কাছে আরও বোধগম্য/সহজবোধ্য করে তোলার মাধ্যমে মানবাধিকার ও স্বাধীনতার অগ্রগতি ঘটায়।">
<!ENTITY aboutTor.getInvolved.label "আপনিও যোগ দিন »">

<!ENTITY aboutTor.newsletter.tagline "টর থেকে পাওয়া সর্বশেষ সব খবর সরাসরি আপনার ইনবক্সে দেখুন।">
<!ENTITY aboutTor.newsletter.link_text "টর নিউজ-এর জন্য সাইন আপ করুন ।">
<!ENTITY aboutTor.donationBanner.freeToUse "আপনার মতো মানুষদের দানের জন্যই Tor বিনামূল্যে ব্যবহার করা যায়">
<!ENTITY aboutTor.donationBanner.buttonA "এখুনি দান করুন! ">

<!ENTITY aboutTor.alpha.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.alpha.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.alpha.bannerDescription "Tor Browser Alpha is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.alpha.bannerLink "Report a bug on the Tor Forum">

<!ENTITY aboutTor.nightly.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.nightly.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.nightly.bannerLink "Report a bug on the Tor Forum">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "POWERED BY PRIVACY:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "RESISTANCE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "CHANGE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "FREEDOM">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "এখনি দান করো">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "Your donation will be matched by Friends of Tor, up to $100,000.">
