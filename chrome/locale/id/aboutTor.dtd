<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Tentang Tor">

<!ENTITY aboutTor.viewChangelog.label "Lihat Catatan Perubahan">

<!ENTITY aboutTor.ready.label "Jelajahi. Secara Privat.">
<!ENTITY aboutTor.ready2.label "Anda siap untuk pengalaman menjelajah yang paling privat di dunia.">
<!ENTITY aboutTor.failure.label "Ada Masalah!">
<!ENTITY aboutTor.failure2.label "Tor tidak dapat digunakan diperamban ini.">

<!ENTITY aboutTor.search.label "Cari dengan DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Pertanyaan?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Periksa Manual Tor Browser kami »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "M">
<!ENTITY aboutTor.torbrowser_user_manual.label "Manual Tor Browser">

<!ENTITY aboutTor.tor_mission.label "The Tor Project adalah US 501(c)(3) organisasi nirlaba yang bertujuan memajukan hak asasi manusia dan kebebasan dengan membuat dan menyebarkan teknologi anominitas dan privasi yang bebas dan terbuka, mendukung keberadaan dan penggunaannya, dan meningkatkan pemahamannya ilmiah dan populernya.">
<!ENTITY aboutTor.getInvolved.label "Ikut Terlibat »">

<!ENTITY aboutTor.newsletter.tagline "Dapatkan berita Tor terbaru langsung dipos-el Anda.">
<!ENTITY aboutTor.newsletter.link_text "Daftar untuk mendapatkan Berita Tor.">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor bebas digunakan karena donasi dari orang-orang seperti Anda.">
<!ENTITY aboutTor.donationBanner.buttonA "Donasi Sekarang">

<!ENTITY aboutTor.alpha.ready.label "Uji. Secara menyeluruh.">
<!ENTITY aboutTor.alpha.ready2.label "Anda siap untuk menguji pengalaman browsing yang paling privat di dunia.">
<!ENTITY aboutTor.alpha.bannerDescription "Tor Browser Alpha adalah versi yang tidak stabil dari Tor Browser yang bisa Anda gunakan untuk meninjau fitur baru, menguji performanya dan memberikan umpan balik sebelum rilis.">
<!ENTITY aboutTor.alpha.bannerLink "Laporkan sebuah bug pada Tor Forum">

<!ENTITY aboutTor.nightly.ready.label "Uji. Secara menyeluruh.">
<!ENTITY aboutTor.nightly.ready2.label "Anda siap untuk menguji pengalaman browsing yang paling privat di dunia.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly merupakan versi tidak stabil dari Tor Browser yang Anda bisa gunakan untuk melihat fitur baru, mencoba kemampuannya dan memberi tanggapan sebelum rilis.">
<!ENTITY aboutTor.nightly.bannerLink "Laporkan sebuah bug pada Tor Forum">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "POWERED BY PRIVACY:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "RESISTANCE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "CHANGE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "FREEDOM">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "DONASI SEKARANG">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "Donasi Anda akan dicocokkan oleh Friends of Tor hingga $100,000.">
